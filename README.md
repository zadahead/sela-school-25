# Sela-React-Class-25

React is great. 
This project will have all of the developement we will make throughout the sessions 
so you will always have a place to return to and recall what we did.

## Getting started

To get started, first you need to clone this to your system. 

1) on your terminal (CMD) goto the directory you want to install the project. (example: _D:/MyProjects/React_)
2) clone project with `git clone`

> D:/MyProjects/React>
```linux
git clone https://gitlab.com/zadahead/sela-school-25.git
```
3) after you will clone it, you will have a new directory called _sela-school-25_
4) go to your directory with 

> D:/MyProjects/React>
```linux
cd sela-school-25
```

## Install React App

1) inside the cloned directory, you will have _hello-react_ folder. 
2) step inside this folder with `cd hello-react`

> D:/MyProjects/React/sela-school-25>
```linux
cd hello-react
```
3) then you will need to install it will `npm install`

> D:/MyProjects/React/sela-school-25/hello-react>
```linux
npm install
```

## Get latest updates

1) each time we will complete a new session, I will push the changes to git. 
2) to fetch new updates all you need to do is `git pull`

> D:/MyProjects/React/sela-school-25>
```linux
git pull
```

## Special Note

*try not to change this project*, 

other wise it will cause conflicts when you try to fetch new updates. 

best thing is to create you own new project with `npx create-react-app your-project` 

and when needed, simply copy-paste code directly to your project. 

